# terraform-aws-default_tags

A brief demonstration of the new `default_tags` feature for the Terraform AWS provider.

## Resources

Basing off blog post here: https://www.hashicorp.com/blog/default-tags-in-the-terraform-aws-provider

And guide here: https://learn.hashicorp.com/tutorials/terraform/expressions?in=terraform/configuration-language

## Guide

Modify `terraform` and `provider` blocks to include default tags and meet minimums for compatibility.

```hcl
terraform {
  required_version = ">= 0.13.0"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 3.38.0"
    }
  }
}

provider "aws" {
  region = var.aws_region
  default_tags {
    tags = {
      Environment = "Dev"
      Owner       = "Chad Quinlan"
      Team        = "RPT"
      Demo        = "AWS provider default tags"
    }
  }
}
```

Run a `terraform plan` and see the tagging applied:

```text
Terraform will perform the following actions:

  # aws_elb.learn will be created
  + resource "aws_elb" "learn" {
      + arn                         = (known after apply)
      + availability_zones          = (known after apply)
      ...
      + tags_all                    = {
          + "Demo"        = "AWS provider default tags"
          + "Environment" = "Dev"
          + "Owner"       = "Chad Quinlan"
          + "Team"        = "RPT"
        }
      ...
    }

  # aws_instance.ubuntu will be created
  + resource "aws_instance" "ubuntu" {
      + ami                                  = "ami-0d563aeddd4be7fff"
      + arn                                  = (known after apply)
      + associate_public_ip_address          = true
      ...
      + tags_all                             = {
          + "Demo"        = "AWS provider default tags"
          + "Environment" = "Dev"
          + "Owner"       = "Chad Quinlan"
          + "Team"        = "RPT"
        }
      ...
    }

...

Plan: 7 to add, 0 to change, 0 to destroy.
```

## Result

After apply, you can log into the AWS console and view the tags on all your resources.
See below the VPC my config created:

![](images/tags.png)
